<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\MaxDepth;
use App\Repository\InformationEntryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InformationEntryRepository::class)
 */
class InformationEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\OneToMany(targetEntity=Rule::class, mappedBy="entry", orphanRemoval=true, cascade={"persist"})
     */
    private $rules;

    /**
     * @MaxDepth(1)
     * @ORM\OneToMany(targetEntity=InformationEntryLearn::class, mappedBy="entry", orphanRemoval=true)
     */
    private $informationEntryLearns;

    public function __construct()
    {
        $this->rules = new ArrayCollection();
        $this->informationEntryLearns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection|Rule[]
     */
    public function getRules(): Collection
    {
        return $this->rules;
    }

    public function addRule(Rule $rule): self
    {
        if (!$this->rules->contains($rule)) {
            $this->rules[] = $rule;
            $rule->setEntry($this);
        }

        return $this;
    }

    public function removeRule(Rule $rule): self
    {
        if ($this->rules->contains($rule)) {
            $this->rules->removeElement($rule);
            // set the owning side to null (unless already changed)
            if ($rule->getEntry() === $this) {
                $rule->setEntry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InformationEntryLearn[]
     */
    public function getInformationEntryLearns(): Collection
    {
        return $this->informationEntryLearns;
    }

    public function addInformationEntryLearn(InformationEntryLearn $informationEntryLearn): self
    {
        if (!$this->informationEntryLearns->contains($informationEntryLearn)) {
            $this->informationEntryLearns[] = $informationEntryLearn;
            $informationEntryLearn->setEntry($this);
        }

        return $this;
    }

    public function removeInformationEntryLearn(InformationEntryLearn $informationEntryLearn): self
    {
        if ($this->informationEntryLearns->contains($informationEntryLearn)) {
            $this->informationEntryLearns->removeElement($informationEntryLearn);
            // set the owning side to null (unless already changed)
            if ($informationEntryLearn->getEntry() === $this) {
                $informationEntryLearn->setEntry(null);
            }
        }

        return $this;
    }
}
