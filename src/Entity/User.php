<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * /**
     * @var Department
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    private $department;

    /**
     * /**
     * @MaxDepth(1)
     * @var Position
     * @ORM\ManyToOne(targetEntity="Position")
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id")
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity=FeatureAnswer::class, mappedBy="user", orphanRemoval=true)
     */
    private $featureAnswers;

    /**
     * @ORM\OneToMany(targetEntity=InformationEntryLearn::class, mappedBy="user", orphanRemoval=true)
     */
    private $informationEntryStudies;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToMany(targetEntity=Project::class, mappedBy="users")
     */
    private $projects;

    public function __construct()
    {
        $this->featureAnswers = new ArrayCollection();
        $this->informationEntryStudies = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @Groups({"group1", "group2"})
     * @return Department|null
     */
    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection|FeatureAnswer[]
     */
    public function getFeatureAnswers(): Collection
    {
        return $this->featureAnswers;
    }

    public function addFeatureAnswer(FeatureAnswer $featureAnswer): self
    {
        if (!$this->featureAnswers->contains($featureAnswer)) {
            $this->featureAnswers[] = $featureAnswer;
            $featureAnswer->setUser($this);
        }

        return $this;
    }

    public function removeFeatureAnswer(FeatureAnswer $featureAnswer): self
    {
        if ($this->featureAnswers->contains($featureAnswer)) {
            $this->featureAnswers->removeElement($featureAnswer);
            // set the owning side to null (unless already changed)
            if ($featureAnswer->getUser() === $this) {
                $featureAnswer->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InformationEntryLearn[]
     */
    public function getInformationEntryStudies(): Collection
    {
        return $this->informationEntryStudies;
    }

    public function addInformationEntryStudy(InformationEntryLearn $informationEntryStudy): self
    {
        if (!$this->informationEntryStudies->contains($informationEntryStudy)) {
            $this->informationEntryStudies[] = $informationEntryStudy;
            $informationEntryStudy->setUser($this);
        }

        return $this;
    }

    public function removeInformationEntryStudy(InformationEntryLearn $informationEntryStudy): self
    {
        if ($this->informationEntryStudies->contains($informationEntryStudy)) {
            $this->informationEntryStudies->removeElement($informationEntryStudy);
            // set the owning side to null (unless already changed)
            if ($informationEntryStudy->getUser() === $this) {
                $informationEntryStudy->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->addUser($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            $project->removeUser($this);
        }

        return $this;
    }
}
