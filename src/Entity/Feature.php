<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\FeatureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FeatureRepository::class)
 */
class Feature
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=FeatureValue::class, mappedBy="feature", orphanRemoval=true, cascade={"persist"})
     */
    private $featureValues;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToMany(targetEntity=Questionary::class, mappedBy="features")
     */
    private $questionaries;

    public function __construct()
    {
        $this->featureValues = new ArrayCollection();
        $this->questionaries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|FeatureValue[]
     */
    public function getFeatureValues(): Collection
    {
        return $this->featureValues;
    }

    public function addFeatureValue(FeatureValue $featureValue): self
    {
        if (!$this->featureValues->contains($featureValue)) {
            $this->featureValues[] = $featureValue;
            $featureValue->setFeature($this);
        }

        return $this;
    }

    public function removeFeatureValue(FeatureValue $featureValue): self
    {
        if ($this->featureValues->contains($featureValue)) {
            $this->featureValues->removeElement($featureValue);
            // set the owning side to null (unless already changed)
            if ($featureValue->getFeature() === $this) {
                $featureValue->setFeature(null);
            }
        }

        return $this;
    }

    /**
     * @MaxDepth(1)
     * @Groups("test")
     * @return Collection|Questionary[]
     */
    public function getQuestionaries(): Collection
    {
        return $this->questionaries;
    }

    public function addQuestionary(Questionary $questionary): self
    {
        if (!$this->questionaries->contains($questionary)) {
            $this->questionaries[] = $questionary;
            $questionary->addFeature($this);
        }

        return $this;
    }

    public function removeQuestionary(Questionary $questionary): self
    {
        if ($this->questionaries->contains($questionary)) {
            $this->questionaries->removeElement($questionary);
            $questionary->removeFeature($this);
        }

        return $this;
    }
}
