<?php

namespace App\Entity;

use App\Repository\RuleParamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RuleParamRepository::class)
 */
class RuleParam
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity=Rule::class, inversedBy="ruleParams")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $rule;

    /**
     * @ORM\OneToMany(targetEntity=RuleParamValue::class, mappedBy="ruleParam", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $ruleParamValues;

    public function __construct()
    {
        $this->ruleParamValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getRule(): ?Rule
    {
        return $this->rule;
    }

    public function setRule(?Rule $rule): self
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * @return Collection|RuleParamValue[]
     */
    public function getRuleParamValues(): Collection
    {
        return $this->ruleParamValues;
    }

    public function addRuleParamValue(RuleParamValue $ruleParamValue): self
    {
        if (!$this->ruleParamValues->contains($ruleParamValue)) {
            $this->ruleParamValues[] = $ruleParamValue;
            $ruleParamValue->setRuleParam($this);
        }

        return $this;
    }

    public function removeRuleParamValue(RuleParamValue $ruleParamValue): self
    {
        if ($this->ruleParamValues->contains($ruleParamValue)) {
            $this->ruleParamValues->removeElement($ruleParamValue);
            // set the owning side to null (unless already changed)
            if ($ruleParamValue->getRuleParam() === $this) {
                $ruleParamValue->setRuleParam(null);
            }
        }

        return $this;
    }
}
