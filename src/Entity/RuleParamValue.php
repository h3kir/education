<?php

namespace App\Entity;

use App\Repository\RuleParamValueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RuleParamValueRepository::class)
 */
class RuleParamValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=RuleParam::class, inversedBy="ruleParamValues")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $ruleParam;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matching;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRuleParam(): ?RuleParam
    {
        return $this->ruleParam;
    }

    public function setRuleParam(?RuleParam $ruleParam): self
    {
        $this->ruleParam = $ruleParam;

        return $this;
    }

    public function getMatching(): ?string
    {
        return $this->matching;
    }

    public function setMatching(string $matching): self
    {
        $this->matching = $matching;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
