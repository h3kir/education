<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\MaxDepth;
use App\Repository\InformationEntryStudyRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=InformationEntryStudyRepository::class)
 */
class InformationEntryLearn
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="informationEntryStudies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity=InformationEntry::class, inversedBy="informationEntryStudies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $entry;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="learned_at")
     */
    private $learnedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEntry(): ?InformationEntry
    {
        return $this->entry;
    }

    public function setEntry(?InformationEntry $entry): self
    {
        $this->entry = $entry;

        return $this;
    }

    public function getLearnedAt(): ?\DateTimeInterface
    {
        return $this->learnedAt;
    }

    public function setLearnedAt(\DateTimeInterface $learnedAt): self
    {
        $this->learnedAt = $learnedAt;

        return $this;
    }
}
