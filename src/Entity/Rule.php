<?php

namespace App\Entity;

use App\Repository\RuleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RuleRepository::class)
 */
class Rule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $points;

    /**
     * @ORM\ManyToOne(targetEntity=InformationEntry::class, inversedBy="rules")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $entry;

    /**
     * @ORM\OneToMany(targetEntity=RuleParam::class, mappedBy="rule", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $ruleParams;

    public function __construct()
    {
        $this->ruleParams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getEntry(): ?InformationEntry
    {
        return $this->entry;
    }

    public function setEntry(?InformationEntry $entry): self
    {
        $this->entry = $entry;

        return $this;
    }

    /**
     * @return Collection|RuleParam[]
     */
    public function getRuleParams(): Collection
    {
        return $this->ruleParams;
    }

    public function addRuleParam(RuleParam $ruleParam): self
    {
        if (!$this->ruleParams->contains($ruleParam)) {
            $this->ruleParams[] = $ruleParam;
            $ruleParam->setRule($this);
        }

        return $this;
    }

    public function removeRuleParam(RuleParam $ruleParam): self
    {
        if ($this->ruleParams->contains($ruleParam)) {
            $this->ruleParams->removeElement($ruleParam);
            // set the owning side to null (unless already changed)
            if ($ruleParam->getRule() === $this) {
                $ruleParam->setRule(null);
            }
        }

        return $this;
    }
}
