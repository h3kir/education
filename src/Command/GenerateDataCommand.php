<?php

namespace App\Command;

use App\Entity\Department;
use App\Entity\Feature;
use App\Entity\FeatureValue;
use App\Entity\Position;
use App\Entity\Questionary;
use App\Entity\User;
use App\Repository\PositionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateDataCommand extends Command
{
    protected static $defaultName = 'generate-data';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->departments();

        $this->positions();

        $this->users();

        $this->features();

        $io->success('Все сгенерилось');

        return 0;
    }

    protected function positions()
    {
        $dir = new Position();
        $dir->setTitle('Директор');

        $it = new Position();
        $it->setTitle('Руководитель IT-отдела');
        $it->setParent($dir);

        $dev = new Position();
        $dev->setTitle('Руководитель отдела разработки');
        $dev->setParent($it);

        $front = new Position();
        $front->setTitle('Программист frontend');
        $front->setParent($dev);

        $back = new Position();
        $back->setTitle('Программист backend');
        $back->setParent($dev);

        $this->em->persist($dir);
        $this->em->persist($it);
        $this->em->persist($dev);
        $this->em->persist($front);
        $this->em->persist($back);
        $this->em->flush();
    }

    protected function departments()
    {
        $dir = new Department();
        $dir->setTitle('Дирекция');

        $it = new Department();
        $it->setTitle('IT-отдел');
        $it->setParent($dir);

        $dev = new Department();
        $dev->setTitle('Отдел разработки системы заказов');
        $dev->setParent($it);

        $this->em->persist($dir);
        $this->em->persist($it);
        $this->em->persist($dev);
        $this->em->flush();
    }

    protected function users()
    {
        $front  = new User();
        $front->setEmail('front@yandex.ru');
        $front->setPosition($this->em->getRepository(Position::class)->findOneBy(['title' => 'Программист frontend']));
        $front->setPassword('1234');
        $front->setDepartment($this->em->getRepository(Department::class)->findOneBy(['title' => 'Отдел разработки системы заказов']));

        $back  = new User();
        $back->setEmail('back@yandex.ru');
        $back->setPosition($this->em->getRepository(Position::class)->findOneBy(['title' => 'Программист backend']));
        $back->setPassword('12345');
        $back->setDepartment($this->em->getRepository(Department::class)->findOneBy(['title' => 'Отдел разработки системы заказов']));




        $this->em->persist($front);
        $this->em->persist($back);
        $this->em->flush();
    }

    protected function features()
    {
        $quastionaryFront = new Questionary();
        $quastionaryFront->setTitle('frontend');
        $quastionaryFront->addPosition($this->em->getRepository(Position::class)->findOneBy(['title' => 'Программист frontend']));

        $quastionaryBack = new Questionary();
        $quastionaryBack->setTitle('backend');
        $quastionaryBack->addPosition($this->em->getRepository(Position::class)->findOneBy(['title' => 'Программист backend']));


        $feature = new Feature();
        $feature->setTitle('Владеете ли анлгийским языком');
        $feature->setCode('eng');
        $feature->setType('yesno');

        $quastionaryBack->addFeature($feature);
        $quastionaryFront->addFeature($feature);

        $this->em->persist($feature);

        $feature = new Feature();
        $feature->setTitle('Какие принципы работы серверов Вы понимаете');
        $feature->setCode('serv');
        $feature->setType('multiselect');
        $feature->addFeatureValue(new FeatureValue('Apache'));
        $feature->addFeatureValue(new FeatureValue('NGINX'));
        $feature->addFeatureValue(new FeatureValue('IIS'));
        $quastionaryBack->addFeature($feature);
        $this->em->persist($feature);

        $feature = new Feature();
        $feature->setTitle('Знание следующих «серверных» языков программирования');
        $feature->setCode('serv_lang');
        $feature->setType('multiselect');
        $feature->addFeatureValue(new FeatureValue('PHP'));
        $feature->addFeatureValue(new FeatureValue('Go'));
        $feature->addFeatureValue(new FeatureValue('Python'));
        $quastionaryBack->addFeature($feature);
        $quastionaryFront->addFeature($feature);
        $this->em->persist($feature);

        $feature = new Feature();
        $feature->setTitle('Как вы оцениваете свой навык написания запросов к БД и проектирования баз данных');
        $feature->setCode('bd');
        $feature->setType('select');
        $feature->addFeatureValue(new FeatureValue('Хорошо'));
        $feature->addFeatureValue(new FeatureValue('Отлично'));
        $feature->addFeatureValue(new FeatureValue('Плохо'));
        $quastionaryBack->addFeature($feature);
        $this->em->persist($feature);

        $feature = new Feature();
        $feature->setTitle('Какими фреймверками вы владееете');
        $feature->setCode('front');
        $feature->setType('multiselect');
        $feature->addFeatureValue(new FeatureValue('Angular'));
        $feature->addFeatureValue(new FeatureValue('React'));
        $feature->addFeatureValue(new FeatureValue('Vue'));
        $quastionaryFront->addFeature($feature);
        $this->em->persist($feature);


        $this->em->persist($quastionaryBack);
        $this->em->persist($quastionaryFront);
        $this->em->flush();
    }
}
