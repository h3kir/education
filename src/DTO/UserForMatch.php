<?php

declare(strict_types=1);


namespace App\DTO;


use App\Entity\FeatureAnswer;
use App\Entity\User;

class UserForMatch
{
    private $data;

    public function __construct(User $user)
    {
        foreach ($user->getFeatureAnswers() as $featureAnswer) { /** @var FeatureAnswer $featureAnswer */
            $featureCode = $featureAnswer->getFeature()->getCode();
            $this->data[$featureCode] = $this->data[$featureCode] ?? [];
            $this->data[$featureCode] []= $featureAnswer->getValue();
        }

        $this->data['position'] = [$user->getPosition()->getId()];
        $this->data['department'] = [$user->getDepartment()->getId()];
    }

    public function get($name): array
    {
        return $this->data[$name] ?? [];
    }
}
