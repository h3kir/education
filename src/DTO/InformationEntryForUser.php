<?php

declare(strict_types=1);


namespace App\DTO;


use App\Entity\InformationEntry;
use App\Entity\InformationEntryLearn;

class InformationEntryForUser
{
    /** @var InformationEntry  */
    private $entry;

    private $learned;

    private $learnedAt;

    public function __construct(InformationEntry $entry, ?InformationEntryLearn $informationEntryLearn)
    {
        $this->entry = $entry;
        $this->learned   = !empty($informationEntryLearn);
        $this->learnedAt = $informationEntryLearn ? $informationEntryLearn->getLearnedAt() : null;
    }

    /**
     * @return InformationEntry
     */
    public function getEntry(): InformationEntry
    {
        return $this->entry;
    }

    /**
     * @return bool
     */
    public function isLearned(): bool
    {
        return $this->learned;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getLearnedAt(): ?\DateTimeInterface
    {
        return $this->learnedAt;
    }
}
