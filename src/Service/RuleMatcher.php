<?php

declare(strict_types=1);


namespace App\Service;


use App\DTO\UserForMatch;
use App\Entity\Rule;
use App\Entity\RuleParam;

class RuleMatcher
{
    /** @var RuleParamMatcher */
    private $ruleParamMatcher;

    public function __construct(RuleParamMatcher $ruleParamMatcher)
    {
        $this->ruleParamMatcher = $ruleParamMatcher;
    }

    /**
     * @TODO: Реализовать другие стратегии сейчас только "И"
     * @param UserForMatch $user
     * @param Rule $rule
     * @return bool
     * @throws \Exception
     */
    public function match(UserForMatch $user, Rule $rule)
    {
        foreach ($rule->getRuleParams() as $ruleParam) { /** @var RuleParam $ruleParam */
            $match = $this->ruleParamMatcher->match($user, $ruleParam);

            if (!$match) {
                return false;
            }
        }

        return true;
    }
}
