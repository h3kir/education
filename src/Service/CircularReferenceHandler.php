<?php


namespace App\Service;


class CircularReferenceHandler
{
    public function __invoke($object, $format, $context)
    {
        return $object->getId();
    }
}