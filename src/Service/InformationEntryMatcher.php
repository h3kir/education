<?php

declare(strict_types=1);


namespace App\Service;


use App\DTO\UserForMatch;
use App\Entity\InformationEntry;
use App\Entity\Rule;

class InformationEntryMatcher
{
    /** @var RuleMatcher  */
    private $ruleMatcher;

    public function __construct(RuleMatcher $ruleMatcher)
    {
        $this->ruleMatcher = $ruleMatcher;
    }

    public function match(UserForMatch $user, InformationEntry $entry)
    {
        $ruleMatcher = $this->ruleMatcher;

        $rules = array_filter($entry->getRules()->toArray(), function(Rule $rule) use($user, $ruleMatcher) {
            return $ruleMatcher->match($user, $rule);
        });

        return $rules;
    }
}
