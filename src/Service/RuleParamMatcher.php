<?php

declare(strict_types=1);


namespace App\Service;


use App\DTO\UserForMatch;
use App\Entity\RuleParam;

class RuleParamMatcher
{
    private $ruleParamValueMatcher;

    public function __construct(RuleParamValueMatcher $ruleParamValueMatcher)
    {
        $this->ruleParamValueMatcher = $ruleParamValueMatcher;
    }

    /**
     * @TODO: Добавить стратегии поведения реализована по умолчанию одна "ИЛИ"
     * @param UserForMatch $user
     * @param RuleParam $ruleParam
     * @throws \Exception
     * @return bool
     */
    public function match(UserForMatch $user, RuleParam $ruleParam)
    {
        foreach ($ruleParam->getRuleParamValues() as $ruleParamValue) {
            $match = $this->ruleParamValueMatcher->match($user, $ruleParamValue);

            if ($match) {
                return true;
            }
        }

        return false;
    }
}
