<?php

declare(strict_types=1);


namespace App\Service;


use App\DTO\UserForMatch;
use App\Entity\RuleParamValue;

class RuleParamValueMatcher
{
    const EQUAL_MATCHING = '=';
    const NOT_EQUAL_MATCHING = '!=';

    /**
     * @param UserForMatch $user
     * @param RuleParamValue $paramValue
     * @return bool|void
     * @throws \Exception
     */
    public function match(UserForMatch $user, RuleParamValue $paramValue)
    {
        if (self::NOT_EQUAL_MATCHING === $paramValue->getMatching()) {
            return ! in_array($paramValue->getValue(), $user->get($paramValue->getRuleParam()->getCode()));
        } elseif (self::EQUAL_MATCHING === $paramValue->getMatching()) {
            return in_array($paramValue->getValue(), $user->get($paramValue->getRuleParam()->getCode()));
        } else {
            throw new \Exception('Неизвестный тип сравнения: ' . $paramValue->getMatching());
        }

    }
}
