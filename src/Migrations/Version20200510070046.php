<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510070046 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rule DROP FOREIGN KEY FK_46D8ACCCBA364942');
        $this->addSql('ALTER TABLE rule ADD CONSTRAINT FK_46D8ACCCBA364942 FOREIGN KEY (entry_id) REFERENCES information_entry (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rule_param_value DROP FOREIGN KEY FK_B44245D99ABB2BCF');
        $this->addSql('ALTER TABLE rule_param_value ADD CONSTRAINT FK_B44245D99ABB2BCF FOREIGN KEY (rule_param_id) REFERENCES rule_param (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rule_param DROP FOREIGN KEY FK_6AC85C49744E0351');
        $this->addSql('ALTER TABLE rule_param ADD CONSTRAINT FK_6AC85C49744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rule DROP FOREIGN KEY FK_46D8ACCCBA364942');
        $this->addSql('ALTER TABLE rule ADD CONSTRAINT FK_46D8ACCCBA364942 FOREIGN KEY (entry_id) REFERENCES information_entry (id)');
        $this->addSql('ALTER TABLE rule_param DROP FOREIGN KEY FK_6AC85C49744E0351');
        $this->addSql('ALTER TABLE rule_param ADD CONSTRAINT FK_6AC85C49744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
        $this->addSql('ALTER TABLE rule_param_value DROP FOREIGN KEY FK_B44245D99ABB2BCF');
        $this->addSql('ALTER TABLE rule_param_value ADD CONSTRAINT FK_B44245D99ABB2BCF FOREIGN KEY (rule_param_id) REFERENCES rule_param (id)');
    }
}
