<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200509153939 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questionary_feature (questionary_id INT NOT NULL, feature_id INT NOT NULL, INDEX IDX_5E43FCBDC49CC05F (questionary_id), INDEX IDX_5E43FCBD60E4B879 (feature_id), PRIMARY KEY(questionary_id, feature_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE questionary_feature ADD CONSTRAINT FK_5E43FCBDC49CC05F FOREIGN KEY (questionary_id) REFERENCES questionary (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE questionary_feature ADD CONSTRAINT FK_5E43FCBD60E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE questionary_feature');
    }
}
