<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200509144111 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE position ADD tree_root INT DEFAULT NULL, ADD parent_id INT DEFAULT NULL, ADD lft INT NOT NULL, ADD lvl INT NOT NULL, ADD rgt INT NOT NULL');
        $this->addSql('ALTER TABLE position ADD CONSTRAINT FK_462CE4F5A977936C FOREIGN KEY (tree_root) REFERENCES position (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE position ADD CONSTRAINT FK_462CE4F5727ACA70 FOREIGN KEY (parent_id) REFERENCES position (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_462CE4F5A977936C ON position (tree_root)');
        $this->addSql('CREATE INDEX IDX_462CE4F5727ACA70 ON position (parent_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE position DROP FOREIGN KEY FK_462CE4F5A977936C');
        $this->addSql('ALTER TABLE position DROP FOREIGN KEY FK_462CE4F5727ACA70');
        $this->addSql('DROP INDEX IDX_462CE4F5A977936C ON position');
        $this->addSql('DROP INDEX IDX_462CE4F5727ACA70 ON position');
        $this->addSql('ALTER TABLE position DROP tree_root, DROP parent_id, DROP lft, DROP lvl, DROP rgt');
    }
}
