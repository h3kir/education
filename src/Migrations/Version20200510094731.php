<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510094731 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE information_entry_learn (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, entry_id INT NOT NULL, learned_at DATETIME NOT NULL, INDEX IDX_95CB4F30A76ED395 (user_id), INDEX IDX_95CB4F30BA364942 (entry_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE information_entry_learn ADD CONSTRAINT FK_95CB4F30A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE information_entry_learn ADD CONSTRAINT FK_95CB4F30BA364942 FOREIGN KEY (entry_id) REFERENCES information_entry (id)');
        $this->addSql('DROP TABLE information_entry_study');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE information_entry_study (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, entry_id INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_1BBA4425BA364942 (entry_id), INDEX IDX_1BBA4425A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE information_entry_study ADD CONSTRAINT FK_1BBA4425A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE information_entry_study ADD CONSTRAINT FK_1BBA4425BA364942 FOREIGN KEY (entry_id) REFERENCES information_entry (id)');
        $this->addSql('DROP TABLE information_entry_learn');
    }
}
