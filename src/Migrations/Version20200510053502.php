<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510053502 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rule (id INT AUTO_INCREMENT NOT NULL, entry_id INT NOT NULL, points INT NOT NULL, INDEX IDX_46D8ACCCBA364942 (entry_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE information_entry (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rule_param_value (id INT AUTO_INCREMENT NOT NULL, rule_param_id INT NOT NULL, matching VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_B44245D99ABB2BCF (rule_param_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rule_param (id INT AUTO_INCREMENT NOT NULL, rule_id INT NOT NULL, code VARCHAR(255) NOT NULL, INDEX IDX_6AC85C49744E0351 (rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rule ADD CONSTRAINT FK_46D8ACCCBA364942 FOREIGN KEY (entry_id) REFERENCES information_entry (id)');
        $this->addSql('ALTER TABLE rule_param_value ADD CONSTRAINT FK_B44245D99ABB2BCF FOREIGN KEY (rule_param_id) REFERENCES rule_param (id)');
        $this->addSql('ALTER TABLE rule_param ADD CONSTRAINT FK_6AC85C49744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rule_param DROP FOREIGN KEY FK_6AC85C49744E0351');
        $this->addSql('ALTER TABLE rule DROP FOREIGN KEY FK_46D8ACCCBA364942');
        $this->addSql('ALTER TABLE rule_param_value DROP FOREIGN KEY FK_B44245D99ABB2BCF');
        $this->addSql('DROP TABLE rule');
        $this->addSql('DROP TABLE information_entry');
        $this->addSql('DROP TABLE rule_param_value');
        $this->addSql('DROP TABLE rule_param');
    }
}
