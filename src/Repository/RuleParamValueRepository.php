<?php

namespace App\Repository;

use App\Entity\RuleParamValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RuleParamValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuleParamValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuleParamValue[]    findAll()
 * @method RuleParamValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuleParamValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuleParamValue::class);
    }

    // /**
    //  * @return RuleParamValue[] Returns an array of RuleParamValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RuleParamValue
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
