<?php

namespace App\Repository;

use App\Entity\FeatureAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FeatureAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeatureAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeatureAnswer[]    findAll()
 * @method FeatureAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeatureAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeatureAnswer::class);
    }

    // /**
    //  * @return FeatureAnswer[] Returns an array of FeatureAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FeatureAnswer
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
