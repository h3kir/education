<?php

namespace App\Repository;

use App\Entity\InformationEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InformationEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method InformationEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method InformationEntry[]    findAll()
 * @method InformationEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformationEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InformationEntry::class);
    }

    // /**
    //  * @return InformationEntry[] Returns an array of InformationEntry objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InformationEntry
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
