<?php

namespace App\Repository;

use App\Entity\RuleParam;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RuleParam|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuleParam|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuleParam[]    findAll()
 * @method RuleParam[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuleParamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuleParam::class);
    }

    // /**
    //  * @return RuleParam[] Returns an array of RuleParam objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RuleParam
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
