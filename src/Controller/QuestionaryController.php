<?php

declare(strict_types=1);


namespace App\Controller;


use App\Entity\Feature;
use App\Entity\FeatureAnswer;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;

class QuestionaryController
{
    public function get(EntityManagerInterface $em, Request $request, SerializerInterface $serializer)
    {
        $userId = $request->get('user_id');

        /** @var User $user */
        $user = $em->find(User::class, $userId);

        if (!$user) {
            throw new HttpException(404, 'Пользователь не найден');
        }

        $questionary = $user->getPosition()->getQuestionary();

        if (!$questionary) {
            throw new HttpException(404, 'Анкета не найдена');
        }

        return new JsonResponse($serializer->serialize($questionary, 'json'), 200, [], true);
    }

    public function save(EntityManagerInterface $em, Request $request)
    {
        $userId = $request->get('user_id');

        /** @var User $user */
        $user = $em->find(User::class, $userId);

        if (!$user) {
            throw new HttpException(404, 'Пользователь не найден');
        }

        $questionary = $user->getPosition()->getQuestionary();

        $body = json_decode($request->getContent(), true);

        foreach ($body['answers'] as $answer) {
            $featureAnswer = new FeatureAnswer();
            $featureAnswer->setUser($user);
            $featureAnswer->setFeature($em->getReference(Feature::class, $answer['feature_id']));
            $featureAnswer->setValue((string)$answer['value']);

            $em->persist($featureAnswer);
        }

        $em->createQueryBuilder()->delete(FeatureAnswer::class, 'a')
            ->where('a.user=:user')
            ->setParameter('user', $user)
            ->getQuery()->execute();

        $em->flush();

        return new JsonResponse();
    }

}
