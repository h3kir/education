<?php

declare(strict_types=1);


namespace App\Controller;


use App\DTO\InformationEntryForUser;
use App\DTO\UserForMatch;
use App\Entity\InformationEntry;
use App\Entity\InformationEntryLearn;
use App\Entity\Rule;
use App\Entity\RuleParam;
use App\Entity\RuleParamValue;
use App\Entity\User;
use App\Service\InformationEntryMatcher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;

class InformationEntryController
{
    public function get(EntityManagerInterface $em, Request $request, SerializerInterface $serializer, $id)
    {
        $userId = $request->get('user_id');

        /** @var User $user */
        $user = $em->find(User::class, $userId);

        if (!$user) {
            throw new HttpException(404, 'Пользователь не найден');
        }

        $informationEntry = $em->find(InformationEntry::class, $id);

        if (!$informationEntry) {
            throw new HttpException(404, 'Квантант не найден');
        }

        $learn = $em->getRepository(InformationEntryLearn::class)
            ->findOneBy(['user' => $user, 'entry' => $informationEntry]);

        $dto = new InformationEntryForUser($informationEntry, $learn);

        return new JsonResponse($serializer->serialize($dto, 'json'), 200, [], true);
    }

    public function save(EntityManagerInterface $em, Request $request, ?int $id)
    {
        $userId = $request->get('user_id');

        /** @var User $user */
        $user = $em->find(User::class, $userId);

        if (!$user) {
            throw new HttpException(404, 'Пользователь не найден');
        }

        $body = json_decode($request->getContent(), true);

        if ($request->isMethod('PUT')) {
            if (empty($id)) {
                throw new HttpException(400, 'Не передан id');
            }

            $informationEntry = $em->find(InformationEntry::class, $id);
            if (!$informationEntry) {
                throw new HttpException(404, 'Квант не найден');
            }

            $em->createQueryBuilder()->delete(Rule::class, 'r')
                ->where('r.entry=:entry')
                ->setParameter('entry', $informationEntry)
                ->getQuery()->execute();

        } else {
            $informationEntry = new InformationEntry();
        }

        $informationEntry->setTitle($body['title']);
        $informationEntry->setLink($body['link']);

        foreach ($body['rules'] as $rule) {
            $ruleObj = new Rule();
            $ruleObj->setPoints($rule['points']);

            foreach ($rule['params'] as $param) {
                $paramObj = new RuleParam();
                $paramObj->setCode($param['code']);

                foreach ($param['values'] as $value) {
                    $paramValue = new RuleParamValue();
                    $paramValue->setValue((string)$value['value']);
                    $paramValue->setMatching($value['matching']);

                    $paramObj->addRuleParamValue($paramValue);
                }

                $ruleObj->addRuleParam($paramObj);
            }

            $informationEntry->addRule($ruleObj);
        }

        $em->persist($informationEntry);
        $em->flush();

        return new JsonResponse();
    }

    public function list(EntityManagerInterface $em, Request $request, SerializerInterface $serializer)
    {
        $informationEntries = $em->getRepository(InformationEntry::class)->findAll();

        return new JsonResponse($serializer->serialize($informationEntries, 'json'), 200, [], true);
    }

    public function delete(EntityManagerInterface $em, $id)
    {
        $informationEntry = $em->find(InformationEntry::class, $id);

        if (!$informationEntry) {
            throw new HttpException(404, 'Квантант не найден');
        }

        $em->remove($informationEntry);
        $em->flush();

        return new JsonResponse();
    }

    public function user(EntityManagerInterface $em, Request $request, SerializerInterface $serializer, InformationEntryMatcher $matcher)
    {
        $informationEntries = $em->getRepository(InformationEntry::class)->findAll();

        $userId = $request->get('user_id');

        /** @var User $user */
        $user = $em->find(User::class, $userId);

        if (!$user) {
            throw new HttpException(404, 'Пользователь не найден');
        }

        $userForMatch = new UserForMatch($user);

        $filteredForUserEntries =  array_filter($informationEntries, function(InformationEntry $entry) use($userForMatch, $matcher) {
            $rules = $matcher->match($userForMatch, $entry);

            return !empty($rules);
        });

        $dtos = array_map(function(InformationEntry $informationEntry) use ($user, $em){
            $learn = $em->getRepository(InformationEntryLearn::class)
            ->findOneBy(['user' => $user, 'entry' => $informationEntry]);

            return new InformationEntryForUser($informationEntry, $learn);
        }, $filteredForUserEntries);

        return new JsonResponse($serializer->serialize($dtos, 'json'), 200, [], true);
    }


    public function learn(EntityManagerInterface $em, Request $request, $id)
    {
        $userId = $request->get('user_id');

        $user = $em->find(User::class, $userId);

        if (!$user) {
            throw new HttpException(404);
        }

        $informationEntry = $em->find(InformationEntry::class, $id);
        if (!$informationEntry) {
            throw new HttpException(404, 'Квант не найден');
        }

        $study = new InformationEntryLearn();
        $study->setUser($user);
        $study->setEntry($informationEntry);

        $em->persist($study);
        $em->flush();

        return new JsonResponse();
    }
}
