<?php

declare(strict_types=1);


namespace App\Controller;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;

class UserController
{
    public function profile(EntityManagerInterface $em, Request $request, SerializerInterface $serializer)
    {
        $userId = $request->get('user_id');

        $user = $em->find(User::class, $userId);

        if (!$user) {
            throw new HttpException(404);
        }

        return new JsonResponse($serializer->serialize($user, 'json'), 200, [], true);
    }
}
